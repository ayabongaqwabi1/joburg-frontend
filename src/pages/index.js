import React from "react"
import ProfileHeader from '../components/Profile/Header';
import BindedButton from '../components/Button/Binded';
import Comment  from  '../components/Comment';
import BusinessCard  from '../components/Card/Business';
import EventCard  from '../components/Card/Event';
import Footer from '../components/Footer';
import woman from '../assets/woman.jpg';
import Layout from "../components/layout";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt, faClock, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import config  from '../config';
import './style.scss';

const IndexPage = () => (
  <Layout>
    <ProfileHeader name="Kirsten Fraser" image={woman} />
    <div className="content">
      <aside className="left-sidebar">
          <div className="profile-info blob">
              <section>
                  <div className="heading">
                      <h2>Profile Info</h2>
                  </div>
                  <div>
                    <ul>
                      <li><FontAwesomeIcon icon={faMapMarkerAlt} />Add Current Location</li>
                      <li><FontAwesomeIcon icon={faClock} />Joined Septmeber 2016</li>
                      <li><FontAwesomeIcon icon={faPlusCircle} />Tell us about yourself</li>
                    </ul>
                  </div>
              </section>
              <section>
                  <div className="heading">
                      <h2>Most Liked Categories</h2>
                  </div>
                  <div className="tags">
                    <p>Restaurants & Bars</p>
                    <p>Parenthood</p>
                    <p>Fashion</p>
                    <p>Food</p>
                  </div>
              </section>
          </div>
          <div className="quicklinks blob">
              <section>
                  <div className="heading">
                      <h2>Quick Links</h2>
                  </div>
                  <div>
                    <ul>
                      <li><FontAwesomeIcon icon={faPlusCircle} />Add Reviews </li>
                      <li><FontAwesomeIcon icon={faPlusCircle} />Add to favorites</li>
                      <li><FontAwesomeIcon icon={faPlusCircle} />Share</li>
                    </ul>
                  </div>
              </section>
          </div>
      </aside>
      <main>
        <div className="nav-bar">
          <div className="title">
            <p>MY REVIEWS</p>
          </div>
          <div className="options">
            <BindedButton options={config.navOptions} />
          </div>
        </div>
        <div className="comments">
          {config.comments.map( comment => <Comment  {...comment}  />)}
        </div>
      </main>
      <aside className="right-sidebar">
          <div className="events blob">
              <section>
                  <div className="heading">
                      <h2>Upcoming Events</h2>
                  </div>
                  <div className="event-cards">
                      {config.events.map(event => <EventCard {...event} />)}
                  </div>
              </section>
              <div className="cta">
                <a href="#"> View All</a>
              </div>
          </div>
          <div className="featured-businesses blob">
              <section>
                  <div className="heading">
                      <h2>Feaured Businesses</h2>
                  </div>
                  <div className="event-cards">
                      {config.featuredBusinesses.map(event => <BusinessCard {...event} />)}
                  </div>
              </section>
              <div className="cta">
                <a href="#"> View All</a>
              </div>
          </div>
      </aside>
    </div>
    <Footer />
  </Layout>
)

export default IndexPage
