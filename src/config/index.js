import biz from '../assets/biz.jpg';
import office from '../assets/office.jpg';
import woman from '../assets/woman.jpg';
import * as R from 'ramda';
import moment from 'moment';
export default {
    events:[
      {
        name: "Event Name",
        date: new Date('06 February 2018'),
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi "
      },
      {
        name: "Event Name",
        date: new Date('06 February 2018'),
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi "
      },
    ],
    navOptions: [
      {
        title: "Business",
        onClick: () => {}
      },
      {
        title: "Events",
        onClick: () => {}
      },
      {
        title: "Specials",
        onClick: () => {}
      }
    ],
    comments: [
      {
        business:{
          name: "Name of Business",
          image:biz,
        },
        rating: 3,
        date: new Date('06 February 2018'),
        detail: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
         Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.`,
        replies: R.times(R.identity, 6)
      },
      {
        business:{
          name: "Name of Business",
          image: biz,
        },
        rating: 3,
        date: new Date('06 February 2018'),
        detail: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
         Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.`,
        replies: R.times(R.identity, 6)
      },
      {
        business:{
          name: "Name of Business",
          image: biz,
        },
        rating: 3,
        date: new Date('06 February 2018'),
        detail: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
         Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.`,
        replies: R.times(R.identity, 6)
      },
      {
        business:{
          name: "Name of Business",
          image: biz,
        },
        rating: 3,
        date: new Date('06 February 2018'),
        detail: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
         Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.`,
        replies: R.times(R.identity, 6)
      }
    ],
    featuredBusinesses:[
      {
        name: "Name of Business",
        image: office,
        rating: 3,
        date: new Date('06 February 2018'),
        detail: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.`,
      },
      {
        name: "Name of Business",
        image: office,
        rating: 3,
        date: new Date('06 February 2018'),
        detail: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.`,
      },
      {
        name: "Name of Business",
        image: office,
        rating: 3,
        date: new Date('06 February 2018'),
        detail: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.`,
      },
    ],
    footer: [
        {
            heading: "Joburg.co.za",
            subheading: "&nbsp ",
            links: [
                {
                    title: "Suburb",
                    href: "#"
                },
                {
                    title: "Favorites",
                    href: "#"
                },
                {
                    title: "Business Listtings",
                    href: "#"
                },
                {
                    title: "Events",
                    href: "#"
                },
                {
                    title: "Abbout us",
                    href: "#"
                },
                {
                    title: "Profile",
                    href: "#"
                }
            ]
        },
        {
            heading: "Browse by categories",
            
            links: [
                {
                    title: "Most Popular",
                    type: "subheading",
                    href: "#"
                },
                {
                    title: "Business",
                    href: "#"
                },
                {
                    title: "Competitions",
                    href: "#"
                },
                {
                    title: "Entertainment",
                    href: "#"
                },
                {
                    title: "Fashion",
                    href: "#"
                },
                {
                    title: "Food",
                    href: "#"
                },
                {
                    title: "Health & Beauty",
                    href: "#"
                },
                {
                    title: "Home & Decor",
                    href: "#"
                }
            ]
        },
        {
            heading: "Browse  by suburb",
            subheading: "Most Popular",
            links: [
                {
                    title: "Most Popular",
                    type: "subheading",
                    href: "#"
                },
                {
                    title: "Suburb",
                    href: "#"
                },
                {
                    title: "Suburb",
                    href: "#"
                },
                {
                    title: "Suburb",
                    href: "#"
                },
                {
                    title: "Suburb",
                    href: "#"
                },
                {
                    title: "Suburb",
                    href: "#"
                },
                {
                    title: "Suburb",
                    href: "#"
                },{
                    title: "Suburb",
                    href: "#"
                }
            ],
        }
    ],
  }
  