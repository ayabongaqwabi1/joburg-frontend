import React, { Component } from "react";
import Nav from './Nav';
import 'semantic-ui-css/semantic.min.css'
import './layout.scss';
export default class Layout extends Component {
    render(){
        return(
            <div id="outer-container">
                <Nav />
                <div id="page-wrap">
                    {this.props.children}
                </div>
            </div>
        )
    }
}