import React, { Component } from "react";
import * as R from 'ramda';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar as filledStar, faShareAlt, faChevronDown } from '@fortawesome/free-solid-svg-icons'
import  { faStar as unFilledStar, faComment }  from  '@fortawesome/free-regular-svg-icons';
import './style.scss';

export default class BusineessCard extends Component {
    render(){
        const { rating, name, image, detail, date} = this.props;
        const ratingArray = R.times(R.identity, 4)
        return(
            <div className="business-card">
                <div className="header">
                    <div className="user-image-container">
                        <div className="user-image" style={{background:`url(${image}) no-repeat center center`}}></div>
                    </div>
                    <div className="about">
                        <p>{name}</p>
                        <p className="rating">
                            {ratingArray.map(i => {
                                if(i <= (rating-1)){
                                    return <FontAwesomeIcon icon={filledStar} />
                                }
                                return <FontAwesomeIcon icon={unFilledStar} />
                            })}
                        </p>
                        <p className="date">{moment(date).format("DD MMM YYYY").toString()}</p>
                    </div>
                </div>
                <div className="detail">
                    <p>{detail}</p>
                </div>
            </div>
        )
    }
}