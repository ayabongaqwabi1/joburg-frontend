import React, { Component } from "react";
import moment from 'moment';
import  './style.scss';

export default class EventCard extends Component {
    render(){
        const {  name, date, description } =  this.props;
        return(
            <div className="event-card">
                <h4>{name}</h4>
                <p className="date">{moment(date).format("DD MMM YYYY").toString()}</p>
                <p className="desc">{description.substring(0, 60)}...</p>
            </div>
        )
    }
}