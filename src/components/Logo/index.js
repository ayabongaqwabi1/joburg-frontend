import React, { Component } from "react";
import logoIcon from '../../assets/logo.svg';

export default class Logo extends Component {
    render(){
        return(<img src={logoIcon} alt='Logo' />)
    }
}