import React, { Component } from "react";
import * as R from 'ramda';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar as filledStar, faShareAlt, faChevronDown } from '@fortawesome/free-solid-svg-icons'
import  { faStar as unFilledStar, faComment }  from  '@fortawesome/free-regular-svg-icons';
import './style.scss';

export default class Comment extends Component {
    render(){
        const { rating, business, detail, date, replies } = this.props;
        const ratingArray = R.times(R.identity, 4)
        console.log(ratingArray)
        return(
            <div className="comment">
                <div className="header">
                    <div className="user-image-container">
                        <div className="user-image" style={{background:`url(${business.image}) no-repeat center center`}}></div>
                    </div>
                    <div className="about">
                        <p>{business.name}</p>
                        <p className="rating">
                            {ratingArray.map(i => {
                                if(i <= (rating-1)){
                                    return <FontAwesomeIcon icon={filledStar} />
                                }
                                return <FontAwesomeIcon icon={unFilledStar} />
                            })}
                        </p>
                        <p className="date">{moment(date).format("DD MMM YYYY").toString()}</p>
                    </div>
                </div>
                <div className="detail">
                    <p>{detail}</p>
                </div>
                <div className="options">
                    <div  className="add-comment">
                        <p>
                            <FontAwesomeIcon icon={faComment} /> <span className="desktop-only"> Comment</span>
                        </p>
                    </div>
                    <div  className="share">
                        <p>
                            <FontAwesomeIcon icon={faShareAlt} /> <span className="desktop-only"> Share</span>
                        </p>
                    </div>
                    <div  className="comment-count">
                        <p>
                            <span> {replies.length} Comments</span><FontAwesomeIcon icon={faChevronDown } />
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}