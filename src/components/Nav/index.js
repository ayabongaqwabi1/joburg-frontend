import React, { Component } from "react";
import Logo from '../Logo';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch,faUserCircle} from '@fortawesome/free-solid-svg-icons'
import { slide as Menu } from "react-burger-menu";
import './style.scss';

export default class Nav extends Component {
    render(){
        return(
            <div>
                <nav className="non-sidebar-nav">
                    <div className="logo-container">
                        <Logo />
                    </div>
                    <div className="nav-links-container desktop-only">
                        <ul>
                            <li>Discover</li>
                            <li>Favourites</li>
                            <li>Business Owner</li>
                            <li>About Us</li>
                            <li><FontAwesomeIcon icon={faUserCircle} /></li>
                            <li><FontAwesomeIcon icon={faSearch} /></li>
                        </ul>
                    </div>
                </nav>
                <div className="mobile-only">
                    <Menu>
                        <ul>
                            <li>Discover</li>
                            <li>Favourites</li>
                            <li>Business Owner</li>
                            <li>About Us</li>
                            <li>Profile <FontAwesomeIcon icon={faUserCircle} /></li>
                            <li>Search <FontAwesomeIcon icon={faSearch} /></li>
                        </ul>
                    </Menu>
                </div>
            </div>
                
        )
    }
}