import React, { Component } from "react";
import './style.scss';

export default class BindedButton extends Component {
    constructor(){
        super();
        this.onClick = this.onClick.bind(this)
        this.state = {
            activeButonIndex : 0,
        }
    }

    onClick(buttonIndex){
        const onClickFunction = this.props.options[buttonIndex].onClick;
        this.setState({ activeButonIndex: buttonIndex})
        onClickFunction();
    }

    render(){
        const { options } = this.props;
        return(
            <div className="binded-button-container">
                {options.map( (option,  index) => {
                    const { title } = option;
                    const buttonClass = index ===  this.state.activeButonIndex ? "binded-button active" : "binded-button";
                    return (
                        <div className={buttonClass} onClick={() => this.onClick(index)}>
                            <p>{title}</p>
                        </div>
                    )
                })}
            </div>
        )
    }
}