import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCamera} from '@fortawesome/free-solid-svg-icons';
import { Dropdown, Header, Icon } from 'semantic-ui-react';
import './style.scss';

const options = [
    {
      key: 'client-reviews',
      text: 'Client Reviews',
      value: 'client-reviews',
      content: 'Client Reviews',
    },
    {
      key: 'favourites',
      text: 'Favourites',
      value: 'favourites',
      content: 'Favourites',
    },
    {
      key: 'activit-feed',
      text: 'Activity Feed',
      value: 'activity-feed',
      content: 'Activity Feed',
    },
    {
        key: 'history',
        text: 'History',
        value: 'history',
        content: 'History',
    },
  ]
  
  const Settings= () => (
    <Header as='h4'>
      <Icon name='setting' />
      <Header.Content>
        <Dropdown
          inline
          options={options}
          defaultValue={options[0].value}
        />
      </Header.Content>
    </Header>
  )

export default class ProfileHeader extends Component {
    render(){
        const { image, name } = this.props;
        return(
             <header>
                 <div className="name-container">
                    <div className="empty"><div></div></div>
                    <div className="name">
                        <h2>{name}</h2>
                    </div>
                 </div>
                 <div className="options-container">
                    <div className="picture">
                        <div className="picture-box" style={{background: `url(${image}) no-repeat center center`}}>
                            <FontAwesomeIcon icon={faCamera} />
                        </div>
                    </div>
                    <div className="options desktop-only">
                        <ul>
                            <li>Client Reviews</li>
                            <li>Favourites</li>
                            <li>Activity Feed</li>
                            <li>History</li>
                        </ul>
                        <div className="settings">
                            <p>Account Settings</p>
                        </div>
                    </div>
                    <div className="options mobile-only">
                        <Settings />
                    </div>
                 </div>
             </header>   
        )
    }
}