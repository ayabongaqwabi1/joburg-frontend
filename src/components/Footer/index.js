import React, { Component } from "react";
import './style.scss';
import Logo from '../Logo';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPinterestP, faGooglePlusG, faFacebookF, faTwitter, faLinkedinIn, faInstagram } from '@fortawesome/free-brands-svg-icons';
import config from '../../config';

const footerConfig = config.footer;

const generateColumn = (column)  =>  {
    const  { heading, subheading, links} = column;
    return (
        <div className='column'>
            <p className="heading">
                {heading}
            </p>
            <ul>
                {links.map( link =>{
                    if(link.type === "subheading"){
                        return(
                            <li className="subheading">{link.title}</li>
                        )
                    }
                    return(
                        <li><a href={link.href}>{link.title}</a></li>
                    )
                })}
            </ul>
        </div>
    )
}

export default class Footer extends Component {
    render(){
        return(
            <div className="footer">
                <footer>
                    <div className="links">
                            {footerConfig.map(column => generateColumn(column))}
                    </div>
                    <div className="advertise">
                            <p><a href="#">Get in touch</a></p>
                            <button className="btn"> Advertise with us</button>
                            <Logo />
                    </div>
                </footer>
                <div className="copyright-container">
                    <div className="copyright">
                        <p><a href="#"> © Copyright 2018</a></p>
                        <p><a href="#">Terms & Conditions</a></p>
                        <p><a href="#">Privacy</a></p>
                    </div>
                    <div className="social">
                        <FontAwesomeIcon icon={faPinterestP} />
                        <FontAwesomeIcon icon={faLinkedinIn} />
                        <FontAwesomeIcon icon={faInstagram} />
                        <FontAwesomeIcon icon={faGooglePlusG} />
                        <FontAwesomeIcon icon={faTwitter} />
                        <FontAwesomeIcon icon={faFacebookF} />
                    </div>
                </div>
            </div>
        )
    }
}